package com.codefactorypdabrowski.puredi.activity

import androidx.appcompat.app.AppCompatActivity

/**
 * All activities should extend base activity
 */
abstract class BaseActivity : AppCompatActivity()